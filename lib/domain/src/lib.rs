//! Domain-specific types, traits, functionality, etc.

#![deny(clippy::future_not_send)]

use std::fmt::Display;

#[derive(Clone, Debug)]
pub struct Package {
    name: ProjectName,
    version: PackageVersion,
}
impl Package {
    pub fn name(&self) -> &ProjectName {
        &self.name
    }

    pub fn version(&self) -> &PackageVersion {
        &self.version
    }
}

#[derive(Clone, Debug)]
pub struct ProjectName(String);

impl Display for ProjectName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Clone, Debug)]
pub struct PackageVersion(String);

impl Display for PackageVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}
