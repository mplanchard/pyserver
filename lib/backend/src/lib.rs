//! Common Backend Traits and Functionality

#![deny(clippy::future_not_send)]

pub mod error;
use async_trait::async_trait;
use bytes::Bytes;
use domain::{Package, PackageVersion, ProjectName};

pub use error::Error;
use futures::Stream;

pub type Result<T> = std::result::Result<T, Error>;

#[async_trait]
pub trait PackageBackend {
    async fn add_package(
        &self,
        package: Package,
        content: impl Stream<Item = Result<Bytes>>,
        signature: Option<impl Stream<Item = Result<Bytes>>>,
    ) -> Result<()>;

    async fn remove_package(&self, package: Package) -> Result<()>;

    async fn find_package(&self, package: Package) -> Result<Option<Package>>;

    async fn project_packages(
        &self,
        name: ProjectName,
    ) -> Result<Box<dyn Iterator<Item = Package>>>;

    async fn project_names(&self) -> Result<Box<dyn Iterator<Item = ProjectName>>>;
}
