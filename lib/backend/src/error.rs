//! Backend Errors

use domain::ProjectName;

#[derive(Debug, thiserror::Error)]
#[non_exhaustive]
pub enum Error {
    #[error("Package already exists: {0}")]
    PackageExists(ProjectName),

    #[error("Error from backend: {0}")]
    BackendError(#[from] Box<dyn std::error::Error + Send + Sync>),
}
